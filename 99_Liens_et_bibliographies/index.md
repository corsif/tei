# Outils TEI
  * Pour encoder :
    * Oxygen
  * Pour visualiser :
    * [TEI Critical Apparatus Toolbox
    * [TEI Cat](http://teicat.huma-num.fr/)
    * [Versioning Machine](http://v-machine.org/) ([téléchargement](http://v-machine.org/download/register/installation/))


# Documentation
  * Site officiel : [http://www.tei-c.org/](http://www.tei-c.org/)
  * Wiki TEI...
  * TEI by Example : http://teibyexample.org/examples/TBED05v00.htm

# Articles et publications
  * ...