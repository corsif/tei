<p class="lead">
	<strong>Bienvenue sur notre guide de transcription</strong>. Sur ce site vous trouverez des informations méthodologiques et pratiques pour mener à bien votre projet de transcription. Vous pouvez aussi consulter les guides de transcrption propre à des projets afin de vous en inspirer&nbsp;!
</p>
<p>
	<strong>La rubrique <a href="00_Transcrire_Kesako">Transcrire kesako ?</a></strong> est une introduction à la problématique de la transcription. Sans constituer un article scientifique, nous y soulevons les éléments qui nous semblent clefs lorsque l'on débute un projet de transcription&nbsp;:</p>
     <ul>
	<li>qu'est-ce que transcrire&nbsp;?</li>
	<li> pourquoi transcrire&nbsp;?</li>
	<li>que transcrire&nbsp;?</li>
	<li>comment transcrire&nbsp;?</li>
     </ul>
<!--
---

### Features

---

<div class="row">
<div class="col-third">

#### Première colonne

* blabla
* blibli

</div>
<div class="col-third">

#### Seconde colonne

* [Auto Syntax Highlighting]()
* [Extend Daux.io with Processors]()
* Full access to the internal API to create new pages programatically
* Work with pages metadata

</div>
<div class="col-third">

#### Troisième colonne

* 100% Mobile Responsive
* 4 Built-In Themes or roll your own
* Functional, Flat Design Style
* Optional code float layout
* Shareable/Linkable SEO Friendly URLs
* Supports Google Analytics and Piwik Analytics

</div>
</div>

---
-->